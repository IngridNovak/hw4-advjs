let loader = `<div class="loader">Loading...</div>`

function checkResponse(url) {
    return fetch(url)
        .then(response => {
            if (!response.ok) {
                throw new Error("Помилка звязку");
            }
            return response.json()
        }).catch(err => {
            console.log(err)
        })
}

function loadFilms() {
    return checkResponse('https://ajax.test-danit.com/api/swapi/films')
        .then(films => {
            return films
        }).catch(error => {
            console.error(error);
        })
}

function showCharacters(characters, filmsCharacterIDs) {

    let paras = document.getElementsByClassName('loader');

    while (paras[0]) {
        paras[0].parentNode.removeChild(paras[0]);
    }

    characters.forEach(character => {
        Object.keys(filmsCharacterIDs).forEach(filmID => {
            if (filmsCharacterIDs[filmID].includes(character.id.toString())) {
                createDOMElement('LI', {'body': character.name}, document.getElementById(filmID));
            }
        });
    })

}

function getInfo(films) {
    let array = [];
    films.forEach(film => {
        let info = {
            episodeId: film.episodeId,
            name: film.name,
            openingCrawl: film.openingCrawl
        };
        array.push(info);
    })
    return array;
}

function showInfo(films) {
    let ul = createDOMElement("UL", [], document.body);
    films.forEach(film => {
        for (let [k, v] of Object.entries(film)) {
            createDOMElement("LI", {'body': `${k}: ${v}`}, ul);
        }
        createDOMElement('UL', {'body': `Characters :  ${loader}`, 'id': film.episodeId}, ul);
    })
}

function createDOMElement(tag, params, parentElement) {
    let elem = document.createElement(tag);
    for (const key in params) {
        elem.setAttribute(key, params[key]);
        if (key === 'body') {
            elem.innerHTML = params[key];
        }
    }
    if (parentElement !== undefined) {
        parentElement.append(elem);
    }
    return elem;
}

function loadData() {

    loadFilms().then(function (films) {

        showInfo(getInfo(films))


        let charactersPromices = []
        let promicesExists = []

        let filmsCharacterIDs = []

        films.forEach(film => {
            film.characters.forEach(character => {
                characterID = character.split("/").at(-1)

                if (filmsCharacterIDs[film.episodeId] === undefined) {
                    filmsCharacterIDs[film.episodeId] = []
                }
                filmsCharacterIDs[film.episodeId].push(characterID)

                if (promicesExists[character] === undefined) {
                    charactersPromices.push(checkResponse(character))
                    promicesExists[character] = true
                }
            })
        })

        Promise.all(charactersPromices).then(characters => {
            showCharacters(characters, filmsCharacterIDs)
        })
    })
}

loadData()